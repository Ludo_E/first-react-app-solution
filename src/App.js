import './App.css';
import { BrowserRouter, Routes,Route } from 'react-router-dom';
import TopBar from './components/TopBar';
import ProductsListComponent from './components/ProductsListComponent';

function App() {
  return (
    <BrowserRouter>
      <TopBar/>
      <Routes>
        <Route path="/" element={<ProductsListComponent/>}/>
      </Routes>
     
    </BrowserRouter>
  );
}

export default App;
