import React from 'react';
import axios from 'axios';
import ProductComponent from "./ProductComponent";
import {Container,Row, Col} from 'react-bootstrap';

export default class ProductsListComponent extends React.Component {
    state = {
        products: []
      }
    componentDidMount() {
        axios.get(`http://localhost:3000/products`)
          .then(res => {
            const products = res.data;
            
            this.setState({ products });
          })
      }
      render() {
        return (
            <>
            <Container className="mt-3">
                <Row>
                    {this.state.products.map(product=>(
                        <Col md={4}>
                            <ProductComponent theproduct={product}/>
                        </Col>
                    ))}
                </Row>
            </Container>
            </>
        );
        }
}
