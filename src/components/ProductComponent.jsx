import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';


function ProductComponent(props) {
  return (
    <Card style={{ width: '18rem' }}>
      <Card.Body>
        <Card.Title>{props.theproduct.name}</Card.Title>
        <Card.Subtitle>{props.theproduct.category.name}</Card.Subtitle>
        <Card.Text>
            {props.theproduct.price} €
        </Card.Text>
        <Button variant="primary">Acheter</Button>
      </Card.Body>
    </Card>
  );
}

export default ProductComponent;